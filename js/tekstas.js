      var config = {
/*
          apiKey: "AIzaSyB_nQLsqp7E3J92cbilt6Cy8oCnO09HmmU",
          authDomain: "tekstas-ca1ab.firebaseapp.com",
          databaseURL: "https://tekstas-ca1ab.firebaseio.com",
          projectId: "tekstas-ca1ab",
          storageBucket: "tekstas-ca1ab.appspot.com",
          messagingSenderId: "239724698149"
*/
           apiKey: "AIzaSyD0VUi_zzpsqdOvBIpYBbLBOTCBQAlqz4A",
           authDomain: "tekstai-44cff.firebaseapp.com",
           databaseURL: "https://tekstai-44cff.firebaseio.com",
           projectId: "tekstai-44cff",
           storageBucket: "",
           messagingSenderId: "29566958161"
      };

      firebase.initializeApp(config);
      database = firebase.database();
      var tekstai = database.ref("Teleloto_Tekstai");
      document.getElementById('irasyti').onclick = function() {
      var checkedDay = document.querySelector('input[name="diena"]:checked');
      var check = -1;
      var daysSolo = [0,1,2,3,4,5,6,7];
      //var date_ = document.getElementById("datepicker1").value + " - " + document.getElementById("datepicker2").value;
      if(checkedDay!=null){
          day_ = checkedDay.value;
          var dateFrom = document.getElementById("datepicker1").value;
          var dateTo = document.getElementById("datepicker2").value;
          var dateSolo = document.getElementById("datepicker3").value;
          var dbDate = "";
          var tx = { tekstas: document.getElementById("text10").value};
          var index = getIndex(day_.toString());
          if((dateFrom!=""&&dateTo!="")&&getIndex(day_.toString())==8) {
            check = 1;
          }
          else if(dateSolo!=""&&(daysSolo.indexOf(getIndex(day_.toString())))!=-1)check = 2;
          else check = -1;
        if(check == 1){

          //durnizmas su data..........
            var dateFrom_ = new Date(dateFrom.toString());
            var dateTo_ = new Date(dateTo.toString());
            if(((dateFrom_.getMonth()+1).toString()).length==1) {
              var month = "0" + (dateFrom_.getMonth()+1).toString();;
            }
            else {
              var month = dateFrom_.getMonth()+1;
            }
            if(((dateFrom_.getDate()).toString()).length==1) {
              var day = "0" + (dateFrom_.getDate()).toString();;
            }
            else {
              var day = dateFrom_.getDate();
            }
            var year = dateFrom_.getFullYear();
            dbDate = year + "-" + month + "-" + day + "  -  ";
            if(((dateTo_.getMonth()+1).toString()).length==1){
              var month = "0" + (dateTo_.getMonth()+1).toString();;
            }
            else {
              var month = dateTo_.getMonth()+1;
            }
            if(((dateTo_.getDate()).toString()).length==1){
              var day = "0" + (dateTo_.getDate()).toString();;
            }
            else {
              var day = dateTo_.getDate();
            }
            year = dateTo_.getFullYear();
            dbDate +=year + "-" + month + "-" + day;
          //durnizmo su data pabaiga.....
          document.querySelector('.alert').style.display = 'block';
          setTimeout(function() {
              document.querySelector('.alert').style.display = 'none';
          }, 3000);
          tekstai.child(index).set({
            tekstas: document.getElementById("text10").value,
            tekstas_nenulos: document.getElementById("text10_1").value,
            day: day_,
            date: dbDate
            });
        }
        else if(check==2){
          //durnizmas su data..........

            var date = new Date(dateSolo.toString());
            if(((date.getMonth()+1).toString()).length==1) {
              var month = "0" + (date.getMonth()+1).toString();;
            }
            else {
              var month = date.getMonth()+1;
            }
            if(((date.getDate()).toString()).length==1) {
              var day = "0" + (date.getDate()).toString();;
            }
            else {
              var day = date.getDate();
            }
            var year = date.getFullYear();
            dbDate = year + "-" + month + "-" + day;
          //durnizmo su data pabaiga.....
          document.querySelector('.alert').style.display = 'block';
          setTimeout(function() {
              document.querySelector('.alert').style.display = 'none';
          }, 3000);
          tekstai.child(index).set({
            tekstas: document.getElementById("text10").value,
            tekstas_nenulos: document.getElementById("text10_1").value,
            day: day_,
            date: dbDate
            });
        }
        else{
          document.getElementById('datepicker1').style.border = '3px solid #FF0000';
          document.getElementById('datepicker2').style.border = '3px solid #FF0000';
          document.getElementById('datepicker3').style.border = '3px solid #FF0000';
        }
      }
        else {
          confirm("Pasirinkite dieną");
      }
      }
      document.getElementById('trinti').onclick = function() {
      var checkedDay = document.querySelector('input[name="diena"]:checked');
      if(checkedDay!=null){
        var day = checkedDay.value;
          setTimeout(function() {
              document.querySelector('.alert').style.display = 'none';
          }, 3000);
          tekstai.child(getIndex(day.toString())).set(null);
          //tekstai.push(tx);
        }
        else {
          confirm("Pasirinkite dieną, kurią norėsite ištrinti");
        }
      }

      var queryRef = tekstai.orderByChild('day')
      queryRef.on('value', function(snapshot) {
          $("#text11").empty();
          var tx = snapshot.val();
          var keys = Object.keys(tx);
          $("#text11").append("Bilietų tekstas, jei nenuloš aukso puodo" + "\n");
          for (var i = 0; i < keys.length; i++) {
              var skaicius = keys.length;
              var k = keys[i];
              document.getElementById('irasai').innerHTML = skaicius;
              var tekstas = tx[k].tekstas;
              var diena = tx[k].day;
              var data = tx[k].date;
              $("#text11").append(data + "\n");
              $("#text11").append(diena + ":\n");
              $("#text11").append(tekstas + "\n" + "\n");
          }
          $("#text11").append("----------" + "\n");
          $("#text11").append("Bilietų tekstas, jei nuloš aukso puodą" + "\n");
          for (var i = 0; i < keys.length; i++) {
              var skaicius = keys.length;
              var k = keys[i];
              document.getElementById('irasai').innerHTML = skaicius;

              var tekstas = tx[k].tekstas_nenulos;
              var txt = tekstas.split('');
              var diena = tx[k].day;
              var data = tx[k].date;
              $("#text11").append(data + "\n");
              $("#text11").append(diena + ":\n");
              $("#text11").append(tekstas + "\n" + "\n");
          }
      });


      function isskleisti(str_) {
        var str = str_.toString();
          var x = document.getElementById("text11");
          if (x.style.display === "block") {
              x.style.display = "none";
              var img = document.getElementById(str);
              img.setAttribute("class", "");
          } else {
              x.style.display = "block";
              var img = document.getElementById(str);
              img.setAttribute("class", "rotated-image");
          }
      }

      function isskleisti2() {
          var x = document.getElementById("aprasymas");
          if (x.style.display === "block") {
              x.style.display = "none";
              var img = document.getElementById("PG");
              img.setAttribute("class", "");
          } else {
              x.style.display = "block";
              var img = document.getElementById("PG");
              img.setAttribute("class", "rotated-image");
          }
      }

      function istrintiViska(){
    if (confirm("At tikrai norite ištrinti?")) {
      tekstai.set(null);
}
location.reload();


        }
    function getIndex(day){
      if(day=="Pirmadienis") return 1;
      else if(day=="Antradienis") return 2;
      else if(day=="Trečiadienis") return 3;
      else if(day=="Ketvirtadienis") return 4;
      else if(day=="Penktadienis") return 5;
      else if(day=="Šeštadienis") return 6;
      else if(day=="Sekmadienis") return 0;
      else if(day=="Pirmadienis-Šeštadienis") return 8;
      else if(day=="Sekmadienis_2") return 0;

    }
    function LoterijosTekstai(){
      var checkedLottery = document.querySelector('input[name="lottery"]:checked');
      if(checkedLottery==null){
        document.getElementById('tbng').checked = true;
      }
        var checkedLottery = document.querySelector('input[name="lottery"]:checked').value;
        return checkedLottery.toString();
    }
    function getTextFromDB(){
      var checkedDay = document.querySelector('input[name="diena"]:checked');
      var day = getIndex(checkedDay.value.toString());
      var queryRef = tekstai.orderByChild('day')
      queryRef.on('value', function(snapshot) {
          var tx = snapshot.val();
          var text;
          var keys = Object.keys(tx);
          for (var i = 0; i < keys.length; i++) {
            if(keys[i]==day){

              text = tx[day].tekstas;
              text_ = tx[day].tekstas_nenulos;
            }
          }
          if(text!=""){
            reverse(text, text_);
          }
      });
    }
