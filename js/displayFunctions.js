
function showDays(){
  var checkedDay = document.querySelector('input[name="sameTextAllWeek"]:checked');
  if(checkedDay!=null){
    var value = checkedDay.value;
    if(value=="different"){
      document.getElementById('skirtingi').style.display = 'block';
      document.getElementById('vienodi').style.display = 'none';
      document.getElementById('dateDiff').style.display = 'none';
      document.getElementById('dateSame').style.display = 'block';
    }
    else if(value=="same"){
      var checkedDay_ = document.querySelector('input[name="diena"]:checked');
      if(checkedDay_!=null){
        var value_  = checkedDay_.value;
        console.log(value_);
        if(value_ == "Pirmadienis-Šeštadienis"){
      document.getElementById('skirtingi').style.display = 'none';
      document.getElementById('vienodi').style.display = 'block';
      document.getElementById('dateDiff').style.display = 'block';
      document.getElementById('dateSame').style.display = 'none';
    }
    else{
      document.getElementById('skirtingi').style.display = 'none';
      document.getElementById('vienodi').style.display = 'block';
      document.getElementById('dateDiff').style.display = 'none';
      document.getElementById('dateSame').style.display = 'block';
}
}
else {
  document.getElementById('skirtingi').style.display = 'none';
  document.getElementById('vienodi').style.display = 'block';
  document.getElementById('dateDiff').style.display = 'none';
  document.getElementById('dateSame').style.display = 'none';
}

  }
}
}
function clearBorder(){
  document.getElementById('datepicker1').style.border = '0px solid #FF0000';
  document.getElementById('datepicker2').style.border = '0px solid #FF0000';
  document.getElementById('datepicker3').style.border = '0px solid #FF0000';
}
function showAdditionalInput(id){
  if(document.getElementById('aditionalInput' + id).checked){
    document.getElementById('textInput' + id).style.display = 'block';
    document.getElementById('sk' + id).style.display = 'block';
  }
  else{
    document.getElementById('textInput' + id).style.display = 'none';
    document.getElementById('sk' + id).style.display = 'none';
  }
}
function countLetters(id) {

   var x = document.getElementById("textInput"+id).value;
   document.getElementById("sk"+id).style.display = "block";
   document.getElementById("sk"+id).innerHTML = "Simbolių skaičius: " + x.length;
}
